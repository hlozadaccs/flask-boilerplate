import os

from core.app import create_app


environment = os.getenv('FLASK_ENV', 'debug')
application = create_app(environment)

if __name__ == '__main__':
    application.run()
