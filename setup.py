from setuptools import find_packages, setup


setup(
    name='flask-boilerplate',
    version='0.0.1',
    packages=find_packages(),
    include_package_data=True,
)
