import os

from core.app import create_app
from core.models import db
from flask_script import Manager
from flask_migrate import Migrate, MigrateCommand


env_name = os.getenv('environment', 'develop')
app = create_app(env_name)


migrate = Migrate(app=app, db=db)
manager = Manager(app=app)
manager.add_command('db', MigrateCommand)


if __name__ == '__main__':
    manager.run()
