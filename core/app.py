from .config import settings
from core.models import db
from flask import Flask, request


def sum(x=0, y=0):
    return x + y


def create_app(environment='develop'):
    app = Flask(__name__, instance_relative_config=True)
    app.config.from_object(settings.get(environment))

    db.init_app(app)

    @app.route('/sum/')
    def sum_endpoint():
        data = request.get_json()
        result = sum(data.get('x'), data.get('y'))
        return {'result': result}, 200

    return app
