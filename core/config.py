import os


class Config(object):
    DEBUG = False
    TESTING = False
    SESSION_COOKIE_SECURE = True
    SQLALCHEMY_DATABASE_URI = os.getenv(
        'DATABASE_URI',
        'postgresql://postgres:postgres@localhost:5432/flask-boilerplate'
    )


class ProductionConfig(Config):
    pass


class DevelopConfig(Config):
    DEBUG = True
    SESSION_COOKIE_SECURE = False


class TestingConfig(Config):
    TESTING = True
    SESSION_COOKIE_SECURE = False


settings = {
    'production': ProductionConfig,
    'develop': DevelopConfig,
    'testing': TestingConfig
}
