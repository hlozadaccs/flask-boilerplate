from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import MetaData
from sqlalchemy.ext.declarative import declared_attr


convention = {
    "ix": 'ix_%(column_0_label)s',
    "uq": "uq_%(table_name)s_%(column_0_name)s",
    "ck": "ck_%(table_name)s_%(constraint_name)s",
    "fk": "fk_%(table_name)s_%(column_0_name)s_%(referred_table_name)s",
    "pk": "pk_%(table_name)s"
}


metadata = MetaData(naming_convention=convention)
db = SQLAlchemy(metadata=metadata)


class BaseMixin(object):
    @declared_attr
    def created_at(cls):
        return db.Column(
            db.DateTime,
            nullable=False,
            default=db.func.now()
        )

    @declared_attr
    def updated_at(cls):
        return db.Column(
            db.DateTime,
            onupdate=db.func.now()
        )

    def save(self):
        db.session.add(self)
        db.session.commit()

    def update(self):
        db.session.commit()

    def delete(self):
        db.session.delete(self)
        db.session.commit()

    @classmethod
    def get_object(cls, id):
        return cls.query.get(id)

    @classmethod
    def get_by(cls, *args, **kwargs):
        return cls.query.filter_by(**kwargs).first()


class Users(BaseMixin, db.Model):
    __tablename__ = 'users'

    id = db.Column(
        db.Integer,
        primary_key=True
    )
    username = db.Column(
        db.String,
        unique=True,
        nullable=False,
    )
    password = db.Column(
        'password',
        db.String,
        nullable=False,
    )
    is_active = db.Column(
        'is_active',
        db.Boolean,
        nullable=False,
        default=True
    )

    def __repr__(self):
        return '<Users {}>'.format(self.username)

    def __str__(self):
        return self.username