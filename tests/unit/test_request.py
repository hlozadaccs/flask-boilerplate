import pytest

from core.app import sum


def test_sum_endpoint(client):
    request_data = {
        'path': '/sum/',
        'json': {
            'x': 1,
            'y': 2
        }
    }

    response = client.get(**request_data)
    data = response.get_json()
    assert data.get('result') == 3
    assert response.status_code == 200


@pytest.mark.parametrize(('x', 'y', 'result'), [
    (1, 2, 3),
    (-1, -2, -3),
    (-1, 2, 1),
    pytest.param(1, 2, 5, marks=pytest.mark.xfail(reason='Return value is not right'))
])
def test_sum_function(x, y, result):
    my_calc = sum(x, y)
    assert result == my_calc
    
